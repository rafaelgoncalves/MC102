/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: Dois números de ponto flutuante (D e A)
 * Saída: Dois números de ponto flutuante (Ce e Ckm)
 * Objetivo: Calcular o perímetro de um planeta em Estádios e
 * em Quilômetros , dado a distância D entre dois pontos, e o 
 * ângulo A formado pelas retas normais que passam por cada ponto */

#include <stdio.h>

int main(void) {
    float D, A;       /* Entradas: Distância D e Ângulo A */
    float Ce, Ckm;    /* Saídas: Circunferência em estádios Ce
                       * e Cinrcunferência em quilômetros Ckm */
    scanf("%f\n%f", &D, &A);

    Ce = 360 * D / A;  /* Calculo do perímetro em estádios */
    Ckm = Ce * 0.1764; /* 1 Estádio = 176.4m = 0.1764km */

    printf("%.1f\n%.1f\n", Ce, Ckm);

    return 0;
}
