/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: Caracteres (X, 0 ou -): v1,v2,v3,v4,v5,v6,v7,v8,v9
 * Saída: A string "X venceu", "0 venceu" ou "empatou".
 * Objetivo: Determinar o vencedor(caso exista) dado o resultado
 * final de um jogo da velha:
 * v1, v2, v3
 * v4, v5, v6
 * v7, v8, v9
 */

#include <stdio.h>

int main(void) {
    char v1, v2, v3, v4, v5, v6, v7, v8, v9, v;
    scanf("%c %c %c %c %c %c %c %c %c", \
            &v1, &v2, &v3, &v4, &v5, &v6, &v7, &v8, &v9);
    if ((((v = v1) == v2) && (v1 == v3)) || \
        (((v = v4) == v5) && (v4 == v6)) || \
        (((v = v7) == v8) && (v7 == v9)) || \
        (((v = v1) == v4) && (v1 == v7)) || \
        (((v = v2) == v5) && (v2 == v8)) || \
        (((v = v3) == v6) && (v3 == v9)) || \
        (((v = v1) == v5) && (v1 == v9)) || \
        (((v = v3) == v5) && (v3 == v7)))  /* Se alguém v venceu*/
        printf("%c venceu\n", v);  /* Imprimir v (quem venceu) */
        
    else
        printf("empatou\n"); /* Se ninguém venceu, imprimir empatou*/
    
    return 0;
}
