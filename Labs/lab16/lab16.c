/* Rafael Gonçalves 
   RA: 186062 
   Objetivos: Implementar dois filtros de imagem (esticar contraste, e passar para 
   escala de cinza) 
   Entrada: Arquivo a ser aplicado os filtros bem como o efeito 'esticar' ou 'cinza'.
   Saída: Arquivo ppm após aplicado o filtro.
   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define X 130
#define Y 130
#define N 4

void lerimagem(char* in, int R[][Y], int G[][Y], int B[][Y], int info[])
{
    /* Abre a imagem em ppm e copia suas informações para 4 matrizes (R, G, B e info). */
    FILE *fp;
    int i, j;
    char lixo[5];
    if (((fp = fopen(in, "r")) == NULL))
       exit(2);
    else{
       fscanf(fp, "%s%d%d%d", lixo, &info[1], &info[2], &info[3]);
       for (i = 0; i < info[2]; i++)
           for (j = 0; j < info[1]; j++)
           {
               fscanf(fp, "%d%d%d", &R[i][j], &G[i][j], &B[i][j]);
           }
       fclose(fp);
    }

}

void escreverimagem(char* out, int R[][Y], int G[][Y], int B[][Y], int info[])
{
    /* Escreve as informações nas matrizes para um arquivo ppm */
   FILE *fp;
   int i, j;
   fp = fopen(out, "w"); 
   fprintf(fp, "P3\n%d %d\n%d\n", info[1], info[2], info[3]);
   for (i = 0; i < info[2]; i++){
       for (j = 0; j < info[1]; j++)
       {
           fprintf(fp, "%d %d %d ", R[i][j], G[i][j], B[i][j]);
       }
    fprintf(fp, "\n");
   }
   fclose(fp);

}

void escaladecinza(char* in, char* out)
{
    /* Converte os pixels para cinza */
    int imagemR[X][Y], imagemG[X][Y], imagemB[X][Y];
    int novaR[X][Y], novaG[X][Y], novaB[X][Y];
    int info[N];
    int i, j;
    
    lerimagem(in, imagemR, imagemG, imagemB, info);

    for (i = 0; i < info[2]; i++)
        for (j = 0; j < info[1]; j++) {
            novaR[i][j] = (imagemR[i][j] + imagemG[i][j] + imagemB[i][j]) / 3;
            novaG[i][j] = novaR[i][j];
            novaB[i][j] = novaR[i][j];
        }

    escreverimagem(out, novaR, novaG, novaB, info);
}

void copiar(char* in, char* out){
    /* Copia a imagem de entrada para o arquivo de saída */
    int imagemR[X][Y], imagemG[X][Y], imagemB[X][Y];
    int novaR[X][Y], novaG[X][Y], novaB[X][Y];
    int info[N];
    int i, j;
    
    lerimagem(in, imagemR, imagemG, imagemB, info);

    for (i = 0; i < info[2]; i++)
        for (j = 0; j < info[1]; j++) {
            novaR[i][j] = imagemR[i][j];
            novaG[i][j] = imagemG[i][j];
            novaB[i][j] = imagemB[i][j];
        }

    escreverimagem(out, novaR, novaG, novaB, info);
}


void esticarcontraste(char* in, char* out)
{
    /* Aumenta o contraste na imagem de entrada */
    int imagemR[X][Y], imagemG[X][Y], imagemB[X][Y];
    int novaR[X][Y], novaG[X][Y], novaB[X][Y];
    int info[N];
    int i, j;

    int Rmax = 0;
    int Gmax = 0;
    int Bmax = 0;
    int Rmin = 256;
    int Gmin = 256;
    int Bmin = 256;

    lerimagem(in, imagemR, imagemG, imagemB, info);

    for (i = 0; i < info[2]; i++ ){
        for (j = 0; j < info[1]; j++) {
    /* loop para setar o mínimo e máximo de cada cor: R, G e B */
        if (imagemR[i][j] > Rmax) Rmax = imagemR[i][j];
        if (imagemR[i][j] < Rmin) Rmin = imagemR[i][j];

        if (imagemG[i][j] > Gmax) Gmax = imagemG[i][j];
        if (imagemG[i][j] < Gmin) Gmin = imagemG[i][j];

        if (imagemB[i][j] > Bmax) Bmax = imagemB[i][j];
        if (imagemB[i][j] < Bmin) Bmin = imagemB[i][j];
        }
    }

    for (i = 0; i < info[2]; i++ ){
        for (j = 0; j < info[1]; j++) {
            novaR[i][j] = floor((float)((imagemR[i][j] - Rmin) * 255.0)/ (float)(Rmax - Rmin));
            novaG[i][j] = floor((float)((imagemG[i][j] - Gmin) * 255.0)/ (float)(Gmax - Gmin));
            novaB[i][j] = floor((float)((imagemB[i][j] - Bmin) * 255.0)/ (float)(Bmax - Bmin));
        }
    }

    escreverimagem(out, novaR, novaG, novaB, info);
}

int main(int argc, char **argv) {
    
    char efeito[20];
    char *arqEntrada = argv[1];
    char *arqSaida = argv[2];
    int i;
    
    if (argc != 3) {
        fprintf(stderr, "Argumentos invalidos. Use:\n");
        fprintf(stderr, "./lab16 <arqEntrada> <arqSaida>\n");
        fprintf(stderr, "Usado:");
        for (i=0; i<argc; i++) {
            fprintf(stderr, " %s", argv[i]);
        }
        fprintf(stderr, "\n");
        return 1;
    }
    
    scanf("%s", efeito);

    switch (efeito[0]) {
        case 'c':
            escaladecinza(arqEntrada, arqSaida);
            break;
        case 'e':
            esticarcontraste(arqEntrada, arqSaida);
            break;
        default:
            copiar(arqEntrada, arqSaida);
            break;
    }

    
    return 0;
    
}
