/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: número de anos n e pib dos anos (p1, p2, ... pn)
 * Saída:  maior media de crescimento foi entre os anos X e Y: Z
 * X = primeiro ano do trienio, Y = ultimo ano do trienio
 * Z = crescimento medio percentual em relação ao primeiro ano.
 * Objetivo: Determinar triênio com maior crescimento.
 */

#include <stdio.h>

int main(void) {
    int n;
    int i, j;    /* índice do primeiro ano e valor, ambos do triênio de maior  */
                              /* crescimento relativo ao ano anterior   */
    float p0, p1, p2, p3, c0, c1, c2, c3, max,ant;
    scanf("%d", &n);
    scanf("%f%f%f", &p0, &p1, &p2); /* primeiro triênio */
    j = 0;
    c2 = (p2 - p1)/p1;
    c1 = (p1 - p0)/p0;
    c0 = 0;
    
    max = (c0+c1+c2)/3;
    for (i = 0; i < (n-3); i++) {
        scanf("%f", &p3);
        c3 = (p3 - p2)/p2;
        ant = (c1 + c2 +c3)/3;
        if (ant > max) { /* trienio atual maior que anterior*/
            max = (c3+c1+c2)/3;
            j = i+1;
        }
        p0 = p1;
        p1 = p2;
        p2 = p3;

        c0 = c1;
        c1 = c2;
        c2 = c3;
    }
    printf("a maior media de crescimento foi entre os anos %d e %d: %.1f\n", \
            j, j+2, max*100);
    return 0;
}
