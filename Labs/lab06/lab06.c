/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: valores aplicados (+) e recebidos(-) por um dos jogadores(Ryu).
 * Saída:  Ryu venceu, Ken venceu ou empatou.
 * Objetivo: Determinar quem ganhou a luta.
 */

#include <stdio.h>

int main(void) {
    int hit, flag, val, ryu;
    scanf("%d", &val);
    do {
        scanf("%d", &hit);
        if (hit < 0) {
            flag++;
        }
        else if (hit >= 0 && flag > 0) { /* acabou o round */
            flag = 0;
            if (val > 0 )
                ryu++;
            else
                ryu--;
            val = 0;
        }

        val += hit;
    }
    while (hit != 0);

    if (ryu > 0) /* ryu ganhou */
        printf("Ryu venceu\n");

    else if (ryu <0) /* ken ganhou */
        printf("Ken venceu\n");

    else /* empatou */
        printf("empatou\n");
    
    return 0;
    }

