/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: Números de ponto flutuante p1, p2, ml e e.
 * Saída: Números de ponto flutuante mp, m e f.
 * Objetivo: Calcular a média segundo as regras da disciplina:
 *
 *      mp = (2*p1+3*p2)/5
 *
 *       m = (3*mp*ml)/(mp+ml)
 *
 *       f = min(5, (m+e)/2), se 2,5 <= M < 5
 *           M, senao
 */

#include <stdio.h>

int main(void) {
    float p1, p2, ml, e; /* Entradas */
    float mp, m, f = 0;  /* Saídas */

    scanf("%f%f%f", &p1, &p2, &ml);
    mp = (2*p1+3*p2)/5.0;
    m = (3.0*mp*ml)/(mp+2*ml);
    f = m;

    if (m < 5 && m >= 2.5) {  /* Ficou de exame */
        scanf("%f", &e);
        f = (m+e)/2.0;
        if (f > 5) 
            f = 5.0;
    }
    printf("%.1f\n%.1f\n%.1f\n", mp, m, f);

    return 0;
}
