/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: Matriz do dia anterior e numero de dias a serem simulados.
 * Saída:  Estado da matriz nos i dias que se passaram.
 * Objetivo: Simular uma matriz com as seguintes regras:
 *   Se X for humano e possuir pelo menos um vizinho zumbi, então X é infectado e se torna um zumbi no dia seguinte;
 *   Se X for zumbi e possuir dois ou mais vizinhos humanos, ele é caçado e morto pelos humanos;
 *   Se X for zumbi e não possuir nenhum vizinho humano, ele morre de fome e fica vazio no dia seguinte;
 *   Se X estiver vazio e possuir exatamente dois vizinhos humanos, independente dos demais vizinhos serem zumbis ou vazio, então um humano nasce em X no dia seguinte.
 *   Se nenhuma das alternativas anteriores for verdade, então X permanece como está.

 */

#include <stdio.h>

#define MAX 80
#define M 80
#define N 80

void simulate(int before[][N], int ans[][N][MAX], int m_max, int n_max, int days){
    /* Simula o passar de n dias segundo as regras já estabelecidas */
    int after[M][N];
    int v[8]; /* Vetor de vizinhos */
    int i, j, m, n, k;
    int zombies = 0, humans = 0;
    for (m = 0; m <= m_max; m++)
        for (n = 0; n <= n_max; n++)
            ans[m][n][0] = before[m][n]; /* Iteração zero é o dia atual */
    k = 1;
    for (i = 1; i <= days; i++){
        for (m = 1; m <= m_max; m++)
            for (n = 1; n <= n_max; n++){
                v[0] = before[m-1][n-1];
                v[1] = before[m-1][n];
                v[2] = before[m-1][n+1];
                v[3] = before[m][n-1];
                v[4] = before[m][n+1];
                v[5] = before[m+1][n-1];
                v[6] = before[m+1][n];
                v[7] = before[m+1][n+1];
                for (j = 0; j < 8; j++){
                    if (v[j] == 2)
                        zombies++; 
                    else if (v[j] == 1)
                        humans++; 
                }
                /* Condições de evolução */
                if (before[m][n] == 1 && zombies > 0)
                    after[m][n] = 2;
                else if (before[m][n] == 2){
                    if (humans >= 2)
                        after[m][n] = 0;
                    else if (humans == 0)
                        after[m][n] = 0;
                    else
                        after[m][n] = before[m][n];
                }
                else if (before[m][n] == 0 && humans == 2)
                    after[m][n] = 1;
                else
                    after[m][n] = before[m][n];
                zombies = humans = 0;
                }
        for (m = 0; m <= m_max; m++)
            for (n = 0; n <= n_max; n++)
                ans[m][n][k] = before[m][n] = after[m][n];
        k++;
    }

    return;
}



int main(void){
    int m, n;
    int days;
    int i, j, k;
    int today[M][N];
    int ans[M][N][MAX];
    /* Coleta de entrada do dia atual e do número de dias a ser simulado*/
    scanf("%d%d%d", &m, &n, &days);
    /* Zerar matrizes para não ficar lixo */
    for (k = 0; k <= days; k++)
        for (i = 0; i <= m+1; i++)
            for(j = 0; j <= n+1; j++){
               today[i][j] = 0; 
               ans[i][j][k] = 0;
            }
    for (i = 1; i <= m; i++)
        for (j = 1; j <= n; j++){
            scanf("%d", &today[i][j]);
        }

    /* Função que simula a evolução da matriz */
    simulate(today, ans, m, n, days);

    /* Imprimir resultados */
    for (k = 0; k <= days; k++){
        printf("iteracao %d\n", k);
        for (i = 1; i <= m; i++){
            for (j = 1; j <= n; j++)
                printf("%d",ans[i][j][k]);
            printf("\n");
        }
    }
    return 0;
}
