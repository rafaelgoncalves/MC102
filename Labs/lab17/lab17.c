/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entradas: número de funcionários, funcionário a ser analisado, matriz de 
 *           hierarquia dos funcionários.
 * Saída: hierarquia relativa ao funcionario 'k'.
 * Objetivo: Encontrar a hierarquia relativa a um funcionário usando uma chamada
 *           recursiva de função.
 */

#include <stdio.h>

#define MAX 31
#define N 100

void sort(int* len, int mat[])
{
    int i;
    int aux, swap = 0;
    do {
        swap = 0;
        for (i = 2; i < *len; i++){
            if ( mat[i-1] > mat[i]){
                aux = mat[i];
                mat[i] = mat[i-1];
                mat[i-1] = aux;
                swap = 1;
            }
        }
    }
    while(swap);
    /* Sair do loop quando não houver mais necessidade de trocar nenhum eelemento */
}

void find(int k, int n, int mat[][MAX], int out[], int* len)
{
    /* Acha recursivamente os funcionários na hierarquia relativa ao funcionário k */
    int i = 0;
	out[*len] = k;
	(*len)++;
    while (i < n){
        if (mat[k][i]){
            find(i, n, mat, out, len);
            }
        i++;
    }
}

int main(void){
    int hier[MAX][MAX];
	int out[N];
    int i, j, func, k;
	int len = 0;
    scanf("%d%d", &func, &k);
    for (i = 0; i < func; i++)
        for (j = 0; j < func ; j++)
            scanf("%d", &hier[i][j]);
     find(k, func, hier, out, &len);
     sort(&len, out);
     for (i = 0; i < len-1; i++)
         printf("%d ", out[i]);
     printf("%d\n", out[i]);
     
     return 0;
}
    
