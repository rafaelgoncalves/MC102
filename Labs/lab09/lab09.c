/* Nome: Rafael Gonçalves
*  RA: 186062
*  Entradas: dicionário de palavras proibidas e senha
*  Saída: Condições que não foram satisfeitas ou ok se todas foram satisfeitas
*  Objetivo: Checar se uma senha corresponde aos seguintes critérios:
*  ter pelo menos 8 caracteres
*  ter ao menos um caractere minusculo
*  ter ao menos um caractere maiusculo
*  ter ao menos um digito
*  ter ao menos um caractere dentre ! @ # $ ?
*  nao ser um palindromo
*  nao conter nenhuma palavra do dicionario */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_WORDS 50
#define MAX_CHARS 20

int eight(char []);
int lo_letter(char []);
int hi_letter(char []);
int has_num(char []);
int has_special(char []);
int is_palindrome(char []);
int has_word_dict(char [], char [][MAX_CHARS+1], int );

int main(void){
    int errors = 0;
    int num, i, j;
    char dict[MAX_WORDS][MAX_CHARS+1];
    char word[MAX_CHARS+1];

    for (i = 0; i < MAX_WORDS; i++) /* Insere zero em todos os elementos dos vetores dict e word */
        for (j = 0; j < MAX_CHARS; j++){
            dict[i][j] = '0';
            word[j] = '0';
        }

    scanf("%d", &num); /* Número de palavras no dicionário de palavras proibidas */
    for(i=0;i<num;i++)
       scanf("%s", dict[i]);

    scanf("%s", word); /* Senha */

    /* Condições da senha */
    if (!eight(word)){
        printf("A senha deve conter pelo menos 8 caracteres\n");
        errors += 1;
    }
    if (!hi_letter(word)){
        printf("A senha deve conter pelo menos uma letra maiuscula\n");
        errors += 1;
    }
    if (!lo_letter(word)){
        printf("A senha deve conter pelo menos uma letra minuscula\n");
        errors += 1;
    }
    if (!has_num(word)){
        printf("A senha deve conter pelo menos um numero\n");
        errors += 1;
    }
    if (!has_special(word)){
        printf("A senha deve conter pelo menos um simbolo\n");
        errors += 1;
    }
    if (is_palindrome(word)){
        printf("A senha e um palindromo\n");
        errors += 1;
    }
    if (has_word_dict(word, dict, num)){
        printf("A senha nao pode conter palavras reservadas\n");
        errors += 1;
    }
    if (errors == 0) /* Condições satisfeitas */
        printf("ok\n");
    return 0;
}

int eight(char word[]){
    /* Retorna 1 se possuir 8 ou mais caracteres */
    int c;
    c = 0;
    while (word[c] != '\0')
        c += 1;
    if (c >= 8)
        return 1;
    else
        return 0;
}

int lo_letter(char word[]){
    /* Retorna 1 se possuir caixa-baixa */
    int c;
    for (c = 0; word[c] != '\0'; c++)
        if (islower(word[c]))
            return 1;
    return 0;
}
int hi_letter(char word[]){
    /* Retorna 1 se possuir caixa-alta */
    int c;
    for (c = 0; word[c] != '\0'; c++)
        if (isupper(word[c]))
            return 1;
    return 0;
}
int has_num(char word[]){
    /* Retorna 1 se possuir número */
    int c;
    for (c = 0; word[c] != '\0'; c++)
        if (isdigit(word[c]))
            return 1;
    return 0;
}

int has_special(char word[]){
   /* Retorna 1 se contiver um dos caracteres especiais enumerados abaixo  */
    int c, i;
    char special[5] = {'!', '?', '#', '$', '@'};
    for (c = 0; word[c] != '\0'; c++)
        for (i = 0; i < 5; i++)
            if (word[c] == special[i])
                return 1;
    return 0;
}

int is_palindrome(char word[]){
    /* Retorna 1 se é palindromo */
    int i, j, error=0;
    int len_word = strlen(word);
	j=len_word-1;
    for (i = 0; i < len_word; i++)
        if (word[i] != word[j--]) /* Compara palavras percorrendo sentidos opostos da mesma string */
            error = 1;
    if (error == 1)
        return 0;
    else
        return 1;
}

int has_word_dict(char word[], char dict[][MAX_CHARS+1], int num){
    /* Retorna 1 se a palavra existe no dicionário de palavras proibidas */
    int i, j, k, l, ans, flag;
    char aux[MAX_CHARS+1];
    int len_dict;
    int len_word = strlen(word);
    flag = 0;
    for (i = 0; i < num; i++) /* Ignorar caixa-alta */
        for (j = 0; j < MAX_CHARS; j++){
            dict[i][j] = tolower(dict[i][j]);
            word[j] = tolower(word[j]);
        }
    for (i = 0; i < num; i++){ /* Para cada palavra word no dicionário de palavras proibidas */
        len_dict = strlen(dict[i]);
        for (j = 0; j < len_word-len_dict; j++){ /* Para cada caractere j da senha */
            for (k = 0, l = j; k < len_dict; k++, l++){ /* Usar string de j até o fim e chamar de aux */
                aux[k] = word[l];
            }
            aux[++k] = '\0';
            ans = memcmp(aux, dict[i], len_dict); /* Comparar se as duas strings aux e word são iguais */
            if (ans == 0){
                flag = 1;
            }
        }
    }
    if (flag)
        return 1;
    else
        return 0;
}
