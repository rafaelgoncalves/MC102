/* Laboratorio 12 - Conjuntos
 * Nome: Rafael Gonçalves
 * RA: 186062
 * Objetivo: Criar funções para as operações de conjuntos.
 */

#include <stdio.h>

/* Funcao: pertence
 *
 * Parametros:
 *   conj: vetor contendo o conjunto de entrada
 *    tam: tamanho do conjunto
 *    num: elemento a ser verificado pertinencia
 *
 * Retorno:
 *   1 se num pertence a conj e 0 caso contrario
 */
int pertence(int conj[], int tam, int num) {
    int i, ans=0;
    for (i = 0; i<tam;i++)
        if (num == conj[i])
            ans = 1;
    return ans;
}

/* Funcao: contido
 *
 * Parametros:
 *   conj1: vetor contendo um conjunto de entrada
 *   conj2: vetor contendo um conjunto de entrada
 *    tam1: tamanho do conjunto conj1
 *    tam2: tamanho do conjunto conj2
 *
 * Retorno:
 *   1 se conj1 esta contido em conj2 e 0 caso contrario
 */
int contido(int conj1[], int conj2[], int tam1, int tam2) {
    int i, ans = 1;
    for (i = 0; i < tam1; i++)
        if (!pertence(conj2, tam2,  conj1[i]))
            ans = 0;
    return ans;
}

/* Funcoes: adicao e subtracao
 *
 * Parametros:
 *   conj: vetor contendo o conjunto que tera incluso ou removido o elemento
 *    tam: tamanho do conjunto
 *    num: elemento a ser adicionado ou removido
 *
 * Retorno:
 *   tamanho do conjunto apos a operacao.
 */
int adicao(int conj[], int tam, int num) {
    if (!pertence(conj, tam, num)){
        conj[tam] = num;
        tam++;
}
    return tam;
}

int subtracao(int conj[], int tam, int num) {
    int i, j = 0, ans = tam;
    if (pertence(conj, tam, num)){
        for (i = 0; i<tam;i++){
            conj[j++] = conj[i];
            if (conj[i] == num)
                j--;
        }
        ans = j;
    }
    return ans;
}

/* Funcoes: uniao, intersecao e diferenca
 *
 * Parametros:
 *   conjRes: vetor contendo o conjunto de saida/resultado da operacao
 *     conj1: vetor contendo o conjunto de entrada do primeiro operando
 *     conj2: vetor contendo o conjunto de entrada do segundo operando
 *      tam1: tamanho do conjunto conj1
 *      tam2: tamanho do conjunto conj2
 *
 * Retorno:
 *   tamanho do conjunto de saida conjRes.
 */
int uniao(int conjRes[], int conj1[], int conj2[], int tam1, int tam2) {
    int i, j, k = 0;
    for(j = 0; j<tam2; j++)
        conjRes[k++] = conj2[j];
    for (i = 0; i<tam1; i++)
        if (!pertence(conjRes, k, conj1[i])){
                conjRes[k++] = conj1[i];
            }
    return k;
}

int intersecao(int conjRes[], int conj1[], int conj2[], int tam1, int tam2) {
    int i, k = 0;
    for(i = 0; i<tam1; i++)
        if (pertence(conj2, tam2, conj1[i]))
            conjRes[k++] = conj1[i];
    return k;
}

int diferenca(int conjRes[], int conj1[], int conj2[], int tam1, int tam2) {
    int i, k = 0;
    for (i = 0; i<tam1; i++)
        if (!pertence(conj2, tam2, conj1[i]))
            conjRes[k++] = conj1[i];
    return k;
}

