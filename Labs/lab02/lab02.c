/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: Seis inteiros v1, v2, v3, v4, v5 e v6
 * Saída: a palavra sim ou a palavra nao
 * Objetivo: Determinar se o triângulo t como o modelo abaixo é
 * um triângulo mágico (soma dos lados iguais => v1 + v2 + v3 = 
 * v3 + v4 + v5 = v5 + v6 + v1)
 *
 *     (v1)
 *  (v2) (v6)
 *(v3) (v4) (v5) */

#include <stdio.h>

int main(void) {
    int v1, v2, v3, v4, v5, v6;
    /* Elementos do triângulo t */
    scanf("%d\n%d\n%d\n%d\n%d\n%d", &v1, &v2, &v3, &v4, &v5, &v6);
    
    if (v1 + v2 + v3 == v3 + v4 + v5 && v1 + v2 + v3  == v5 + v6 + v1)
        printf("sim\n");   /* É um triângulo mágico */
    else
        printf("nao\n");   /* Não é um triângulo mágico */

    return 0;
}
