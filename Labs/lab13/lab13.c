/* Laboratorio 13 - Tetris
 * Nome: Rafael Gonçalves
 * RA: 186062
 * Objetivo: Implementar funções para o funcionamento da versão simplificada de Tetris
 */

#include <stdio.h>

#define ALTURA_TABULEIRO 10
#define LARGURA_TABULEIRO 10

/* Funcao: atualiza_posicao
 *
 * Parametros:
 *     *l: apontador para a largura do bloco que ira cair
 *     *a: apontador para a altura do bloco que ira cair
 *     *x: apontador para a posicao horizontal inicial do bloco que ira cair
 *   desl: deslocamento horizontal a ser aplicado ao bloco (positivo para direita, negativo para a esquerda) 
 *    rot: 1 se deve rotacionar o bloco, 0 caso contrario 
 *
 * Retorno:
 *   NULL
 */
void atualiza_posicao(int *l, int *a, int *x, int desl, int rot) {
    int aux, i, p = 1, dif;
    int mdesl = desl;
    if (rot == 1)
    {
        /* rotação */
        aux = *a;
        *a = *l;
        *l = aux;
        if ((dif = (*x+*l-1-LARGURA_TABULEIRO)) >= 0)
            *x -= dif-1; 
    }
    if (desl < 0){
        /* módulo de desloca, para a iteração no loop de baixo */
        mdesl = -desl;
        p = -1;
    }
    for(i = 0; i < mdesl; i++)
        /* deslocamento */
        if ((*x + p >= 0 && *x + p <= 10)&&(*x + *l + p >= 0 && *x + *l + p <= 10))
            (*x)+=p;

    return;
}

/* Funcao: encontra_y
 *
 * Parametros:
 *    mat: matriz representando o tabuleiro 
 *      l: largura do bloco que ira cair
 *      x: posicao horizontal do bloco que ira cair
 *
 * Retorno:
 *   altura final y do canto inferior esquerdo do bloco apos
 *   este descer o maximo possivel
 */
int encontra_y(int mat[ALTURA_TABULEIRO][LARGURA_TABULEIRO], int l, int x) {
    int y, i, j, stop=0;
    for (i = ALTURA_TABULEIRO - 1; stop == 0 && i >= -1; i--){
        for (j = x; stop == 0 && j < (x+l); j++)
            if (mat[i][j] == 1)
                stop = 1;
    }
    y = i+2;
    return y;
}

/* Funcoes: posicao_final_valida
 *
 * Parametros:
 *      a: altura do bloco que caiu
 *      y: altura final do bloco que caiu
 *
 * Retorno:
 *   1 se o bloco naquela posicao estiver contido dentro do tabuleiro, ou 0 caso contrario.
 */
int posicao_final_valida(int a,  int y) {
    int ans;
    if (a+y <= ALTURA_TABULEIRO)
        ans = 1;
    else 
        ans = 0;
    return ans;
}

/* Funcoes: posiciona_bloco
 *
 * Parametros:
 *    mat: matriz do tabuleiro 
 *      l: largura do novo bloco
 *      a: altura do novo bloco
 *      x: posicao horizontal do novo bloco
 *      y: altura final do novo bloco
 *
 *      Deve preencher com 1s as novas posições ocupadas pelo bloco que caiu
 * Retorno:
 *   NULL
 */
void posiciona_bloco(int mat[ALTURA_TABULEIRO][LARGURA_TABULEIRO], int l, int a, int x, int y) {
    int i, j;
    for (i=y; i < y+a; i++)
        for (j = x; j < x+l; j++)
            mat[i][j] = 1;
    return;
}

/* Funcoes: atualiza_matriz
 *
 * Parametros:
 *    mat: matriz do tabuleiro 
 *
 *         Deve remover as linhas totalmente preenchidas do tabuleiro copiando
 *         linhas posicionadas acima.
 * Retorno:
 *   retorna o numero de linhas totalmente preenchidas que foram removidas apos
 *   a atualizacao do tabuleiro.
 */
int atualiza_matriz(int mat[ALTURA_TABULEIRO][LARGURA_TABULEIRO]) {
    int flag;
    int i, j, k = 0;
    int zeros = 0;
    for (i = 0; i < ALTURA_TABULEIRO; i++){
        flag = 1;
        for (j = 0; j < LARGURA_TABULEIRO; j++)
            if (mat[i][j] != 1)
                flag = 0;
        if (flag == 0){
        /* Linha não será removida */
            for (j = 0; j < LARGURA_TABULEIRO; j++)
                mat[k][j] = mat[i][j];
            k++;
        }
        else
        /* Linha será removida */
            zeros++;
    }
    for (i = 0; i < zeros; i++)
        for (j = 0; j<LARGURA_TABULEIRO; j++)
        /* adiciona linhas com zeros no topo, devido às linhas removidas */
            mat[i+k][j] = 0;
    return zeros;
}

