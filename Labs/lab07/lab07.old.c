/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: valores aplicados (+) e recebidos(-) por um dos jogadores(Ryu).
 * Saída:  Ryu venceu, Ken venceu ou empatou.
 * Objetivo: Determinar quem ganhou a luta considerando os combos:
 * número triangular não perfeito, o golpe vale o dobro,
 * número perfeito, o golpe vale o triplo
 */

#include <stdio.h>

int main(void) {
    int i, m, hit, abs_hit, flag, val, ryu;
    ryu = 0;
    flag = 0;
    m = 0;

    do {
        scanf("%d", &hit);
        abs_hit = hit;
        if (hit < 0) {
            flag++;
            abs_hit = -hit;
        }
        else if (hit >= 0 && flag > 0) { /* acabou o round */
            flag = 0;
            if (val > 0 )
                ryu++;
            else if (val <0)
                ryu--;
            printf("val = %d ryu = %d\n", val, ryu);
            val = 0;
        }
        /* for (i = abs_hit; i > 0; i--) */
        /*     if (abs_hit % i == 0){ */
        /*         m += i; */
        /*     } */

        if ((abs_hit*abs_hit+abs_hit)/2 % 2 == 0){
            for (i = abs_hit; i > 0; i--)
                if (abs_hit % i == 0){
                    m += i;
                }
            if (m == abs_hit){ /* número perfeito */
                hit *= 3;
                printf("%d é perfeito -> %d\n", abs_hit, hit);
            }
            else {
                hit *= 2;
                printf("%d é triangular nao perfeito -> %d\n", abs_hit, hit);
            }
        }


        /* if (m == abs_hit){ #<{(| número perfeito |)}># */
        /*     hit *= 3; */
        /*     printf("%d é perfeito -> %d\n", abs_hit, hit); */
        /* } */
        /*  */
        /* else { */
        /*     m = 0; */
        /*     for (i = 0; m <= abs_hit; i++){ */
        /*         m += i; */
        /*         if (m == abs_hit){  #<{(| número triângular não perfeito |)}># */
        /*             hit *= 2; */
        /*             printf("%d é triangular nao perfeito -> %d\n", abs_hit, hit); */
        /*         } */
        /*     } */
        /* } */
        printf("val %4d + hit %4d = ", val, hit);
        val += hit;
        printf(" val: %d\n", val);
    }
    while (hit != 0);

    if (ryu > 0) /* ryu ganhou */
        printf("Ryu venceu\n");

    else if (ryu <0) /* ken ganhou */
        printf("Ken venceu\n");

    else /* empatou */
        printf("empatou\n");
    
    return 0;
    
    
}

