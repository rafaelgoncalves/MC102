/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entrada: valores aplicados (+) e recebidos(-) por um dos jogadores(Ryu).
 * Saída:  Ryu venceu, Ken venceu ou empatou.
 * Objetivo: Determinar quem ganhou a luta considerando os combos:
 * número triangular não perfeito, o golpe vale o dobro,
 * número perfeito, o golpe vale o triplo
 */

#include <stdio.h>

int main(void) {
    int i, m, hit, triangle, abs_hit, flag, val, ryu;
    ryu = 0;
    flag = 0;
    val = 0;
    m = 0;

    do {
        scanf("%d", &hit);
        abs_hit = hit;
        if (hit < 0) {
            flag++;
            abs_hit = -hit;
        }
        else if (hit >= 0 && flag > 0) { /* acabou o round */
            flag = 0;
            if (val > 0 )
                ryu++;
            else if (val <0)
                ryu--;
            val = 0;
        }

        m = 0;
        triangle = 0;
        for(i = 1; i < abs_hit; i++)
            if (((1+i)*i)/2 == abs_hit) /* soma de i números começando */
                triangle = 1;           /* no 1, igual ao valor de abs_hit */
        if (triangle == 1) {  /* É triangular */
            m = 0;
            for (i = 1; i < abs_hit; i++)
                if (abs_hit % i == 0)
                    m += i;   /* m = soma dos divisores */
            if (m == abs_hit) /* É perfeito */
                hit *= 3;
            else
                hit *= 2;
        }
        val += hit;
    
    }
    while (hit != 0);

    if (ryu > 0) /* ryu ganhou */
        printf("Ryu venceu\n");

    else if (ryu <0) /* ken ganhou */
        printf("Ken venceu\n");

    else /* empatou */
        printf("empatou\n");
    
    return 0;
}
