#include <stdio.h>

int main(void) {
	int hit, abs_hit, triangular, somadiv, i;

	do {
		scanf("%d", &hit);
		abs_hit = hit;
		if(hit < 0) abs_hit *= -1; 
		triangular = 0;
		if(abs_hit == 1) triangular = 1;
		else {
			for(i=1; i < abs_hit; i++)
				if(((1+i)*i)/2 == abs_hit) triangular = 1;
		}
		if(triangular == 1) {
			printf("triangular!\n");
			somadiv = 0;
			for(i = 1; i < abs_hit; i++)
				if(abs_hit % i == 0) somadiv += i;
			if(somadiv == abs_hit){ hit *= 3; printf("Perfeito!");}
			else hit *= 2;
		}
	} while (hit != 0);
	return 0;
}
