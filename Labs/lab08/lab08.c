/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entradas: respectivamente:
 *
 *     n
 *     i pca pcf - n linhas
 *     i pca
 *     0 0
 *
 *     n - numero de evoluções int <= 151
 *     i = identificador int de 1 a 1000000000
 *     pca - poder antes
 *     pcf - poder depois
 *
 * Saída:
 * linhas contendo o pcf das linhas de entrada apenas com i e pca
 * Objetivo: Calcular pcf dados i e pca depois de construído um banco de
 * dados dos coeficientes de multiplicação para cada espécie com base nas
 * primeiras n entradas.
 */

#include <stdio.h>
#include <math.h>

#define MAX 151

int main(void) {
    int input[MAX][3]; /* Entradas i, pca e pcf */
    int i, j, k, already, num, n[MAX], pca; /* Variáveis auxiliares */
    int index[MAX];
    float m[MAX];
    int pcf_out[MAX]; /* Saída pcf */

    /* Zerar variáveis que pressupoem valor inicial zero */
    for (i = 0; i < MAX; i++){
        index[i] = 0;
        m[i] = 0;
    }

    /* Coleta de dados */
    scanf("%d", &num);
    for (i = 0; i < num; i++){
        scanf("%d%d%d", &input[i][0], &input[i][1], &input[i][2]);
    }

    /* Checando números repetidos de monstros  e somando a razao pcf/pca em m*/
    already = 0;
    k = 0;
    for (i = 0; i < num; i++){
        for (j = 0; j < num; j++)
            if (input[i][0] == index[j]){
                already = 1;
                m[j] += (float) input[i][2]/input[i][1];
                n[j] += 1;
            }
        if (already == 0){
            index[k] = input[i][0];
            m[k] = (float) input[i][2]/ input[i][1];
            n[k] = 1;
            k++;
        }

        already = 0;
    }

    /* Cálculo dos multiplicadores dividindo-se m por n */
    for (i = 0; i<num; i++)
        m[i] = m[i] / n[i];

    /* Consulta de dados */
    k = 0;
    while(i != 0 || pca != 0){
        scanf("%d%d", &i, &pca);
        for (j = 0; j < num; j++)
            if (i == index[j])
                pcf_out[k] = ceil(m[j] * pca);
        k++;
    }

    /* Impressão dos dados na tela */
    for (j = 0; j < (k-1); j++)
        printf("%d\n", pcf_out[j]);

    return 0;
}
