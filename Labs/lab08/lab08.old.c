/* Nome: Rafael Gonçalves
 * RA: 186062
 * Entradas: respectivamente:
 *
 *     n
 *     i pca pcf - n linhas
 *     i pca
 *     0 0
 *
 *     n - numero de evoluções int <= 151
 *     i = identificador int de 1 a 1000000000
 *     pca - poder antes
 *     pcf - poder depois
 *
 * Saída:
 * linhas contendo o pcf das linhas de entrada apenas com i e pca
 * Objetivo: Calcular pcf dados i e pca depois de construído um banco de
 * dados dos coeficientes de multiplicação para cada espécie com base nas
 * primeiras n entradas.
 */

#include <stdio.h>
#include <math.h>

#define MAX_IN 151

int main(void) {
    int num, i, pca, pcf, j, k, l;
    int n[MAX_IN], is[MAX_IN];          /* Entradas*/
    float  pcas[MAX_IN], pcfs[MAX_IN];
    int pcfs_out[MAX_IN];               /* Saída */
    long index[MAX_IN]; /* Índice de espécie e respectivo multiplicador */
    float m[MAX_IN];


    /* Zerar variáveis que pressupoem valor inicial zero */
    for (j = 0; j < MAX_IN; j++) {
        index[j] = 0;
        m[j] = 0;
        n[j] = 0;
        is[j] = 0;
    }

    /* Coleta de dados */
    scanf("%d", &num);

    for (j = 0; j < num; j++){
        scanf("%d%d%d", &i, &pca, &pcf);
        is[j] = i;
        pcas[j] = pca;
        pcfs[j] = pcf;
    }

    /* Armazenamento da soma das razões pcf/pca */
    l = 0;
    for (j = 0; j < MAX_IN; j++, l++) {
        for (k = l; k < num; k++) {
            if (index[j] == 0) {
                index[j] = is[k];
                m[j] =  pcfs[k]/pcas[k];
                n[j] = 1;
            }
            else if (index[j] == is[k]) {
                m[j] +=  pcfs[k]/pcas[k];
                n[j] += 1;
            }
        }
    }
    k = 0;

    /* Cálculo dos multiplicadores */
    for (j = 0; j < MAX_IN; j++){
        m[j] /= n[j];
    }

    /* Consulta de dados */
    while(i != 0 || pca != 0){
        scanf("%d%d", &i, &pca);
        for (j = 0; j < MAX_IN; j++)
            if (i == index[j])
                pcfs_out[k] = ceil(m[j] * pca);
        k++;
    }

    /* Impressão dos dados na tela */
    for (j = 0; j < (k-1); j++)
        printf("%d\n", pcfs_out[j]);

    return 0;
}
