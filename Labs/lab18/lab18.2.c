#include <stdio.h>
#include <stdlib.h>

#define MAX 9

/*
 * Funcao: print_sudoku
 * Essa funcao ja esta implementada no arquivo lab18_main.c
 * A funcao imprime o tabuleiro atual do sudoku de forma animada, isto e,
 * imprime o tabuleiro e espera 0.1s antes de fazer outra modificacao.
 * Voce deve chamar essa funcao a cada modificacao na matriz resposta, assim
 * voce tera uma animacao similar a apresentada no enunciado.
 * Essa funcao nao tem efeito na execucao no Susy, logo nao ha necessidade de
 * remover as chamadas para submissao.
 */
void print_sudoku(int resposta[MAX][MAX]);

void fcampo(int m, int n, int campo[])
{
    if (m%3 < 1){
        campo[0] = 0;
        campo[1] = 2;
    }
    else if (m%3 >= 2){
        campo[0] = 6;
        campo[1] = 8;
    }
    else{
        campo[0] = 3;
        campo[1] = 5;
    }
    if (n%3 < 1){
        campo[2] = 0;
        campo[3] = 2;
    }
    else if (n%3 >= 2){
        campo[2] = 6;
        campo[3] = 8;
    }
    else{
        campo[2] = 3;
        campo[3] = 5;
    }
}


void num_possiveis(int mat[MAX][MAX], int ans[MAX][MAX][MAX+1], int ip[MAX][MAX])
{
    int i, j, k, l;
    int  campo[4];
    for (i = 0; i < MAX; i++){
        for (j = 0; j < MAX; j++){
            for (k = 1; k <= MAX;i++)
                ans[i][j][k] = 1;
            ans[i][j][0] = 0;
        }
    }
    for (i = 0; i < MAX; i++)
        for (j = 0; j < 100; j++){
            for (k = 0; k < MAX; k++){
                ans[i][j][mat[i][k]] = 0; /* Mesma coluna */
                ans[i][j][mat[k][i]] = 0; /* Mesma linha */ 
        }
    }
    for (i = 0; i < MAX; i++)
        for (j = 0; j < MAX; j++){
            fcampo(i, j, campo);
            for (k = campo[0]; k <= campo[1]; k++)
                for (l = campo[2]; l <= campo[3]; l++){
                    ans[i][j][mat[k][l]] = 0; /* Mesmo campo */ 
                }
        }
}

void copia(in, m, n, out)
{
    int i, j;
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            out[i][j] = in[i][j];
}

int resolve(int resposta[MAX][MAX]) {
    int ans[MAX][MAX][MAX+1];
    int ip[MAX][MAX];
    int i, j, won = 1;
    /* int copia[MAX][MAX]; */
    /* copia(resposta, MAX, MAX, copia); */
    print_sudoku(reposta);
    num_possiveis(resposta, ans);
    for (i = 0; i < MAX; j++)
        for (j = 0; j < MAX; j++)
            if (resposta[i][j] == 0){
                for (k = 0; k < MAX; k++){
                    resposta[i][j] = ans[i][j][ip[i][j]];
                }
            }
    if (won)
        return 1;
    return 0;
}
