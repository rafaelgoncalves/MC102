/* Nome: Rafael Gonçalves
   RA: 186062
   */

#include <stdio.h>
#include <stdlib.h>

#define MAX 9

/*
 * Funcao: print_sudoku
 * Essa funcao ja esta implementada no arquivo lab18_main.c
 * A funcao imprime o tabuleiro atual do sudoku de forma animada, isto e,
 * imprime o tabuleiro e espera 0.1s antes de fazer outra modificacao.
 * Voce deve chamar essa funcao a cada modificacao na matriz resposta, assim
 * voce tera uma animacao similar a apresentada no enunciado.
 * Essa funcao nao tem efeito na execucao no Susy, logo nao ha necessidade de
 * remover as chamadas para submissao.
 */
void print_sudoku(int resposta[MAX][MAX]);

void  num_possiveis(int mat[MAX][MAX], int m, int n, int ans[])
{
    int i, j, k, a, b;
/*    int flag = 0; */
    for (i = 0; i <= MAX;i++)
        ans[i] = 1;
    ans[0] = 0;
    for (i = 0; i < MAX; i++){
        ans[mat[i][n]] = 0; /* Mesma coluna */
        ans[mat[m][i]] = 0; /* Mesma linha */ 
    }
    a = (m/3)*3;
    b = (n/3)*3;
    for (j = a; j < a+3; j++)
        for (k = b; k < b+3; k++){
            ans[mat[j][k]] = 0; /* Mesmo campo */ 
        }
/*    for (i = 0; i <= MAX; i++)
        if (ans[i] == 1)
            flag = 1;
    return flag;
*/
}

int rec(int resposta[][MAX], int m, int n)
{
    int i, ans;
    int n2 = (n+1)%MAX;
    int m2 = n2? m : (m+1);
    int possiveis[MAX+1];

    if (m == MAX)
    {
        return 1;
    } 
    if (resposta[m][n] != 0)
    {
        return rec(resposta, m2, n2);
    }
    print_sudoku(resposta);    
    num_possiveis(resposta, m, n, possiveis);
    for (i = 1; i <= MAX; i++)
    {
        if (possiveis[i])
        {
            resposta[m][n] = i;
            ans = rec(resposta, m2, n2);
            if (ans == 1)
                return 1;
         }
    }
    resposta[m][n] = 0;
    return 0;
}

/*
 * Funcao: resolve
 * Resolve o Sudoku da matriz resposta.
 * Retorna 1 se encontrar uma resposta, 0 caso contrario
 */
int resolve(int resposta[MAX][MAX]) {
    int ans = rec(resposta, 0,0);
    print_sudoku(resposta);
    return ans;
}
