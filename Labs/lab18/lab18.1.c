#include <stdio.h>
#include <stdlib.h>

#define MAX 9

/*
 * Funcao: print_sudoku
 * Essa funcao ja esta implementada no arquivo lab18_main.c
 * A funcao imprime o tabuleiro atual do sudoku de forma animada, isto e,
 * imprime o tabuleiro e espera 0.1s antes de fazer outra modificacao.
 * Voce deve chamar essa funcao a cada modificacao na matriz resposta, assim
 * voce tera uma animacao similar a apresentada no enunciado.
 * Essa funcao nao tem efeito na execucao no Susy, logo nao ha necessidade de
 * remover as chamadas para submissao.
 */
void print_sudoku(int resposta[MAX][MAX]);

void fcampo(int m, int n, int campo[])
{
    if (m%3 < 1){
        campo[0] = 0;
        campo[1] = 2;
    }
    else if (m%3 >= 2){
        campo[0] = 6;
        campo[1] = 8;
    }
    else{
        campo[0] = 3;
        campo[1] = 5;
    }
    if (n%3 < 1){
        campo[2] = 0;
        campo[3] = 2;
    }
    else if (n%3 >= 2){
        campo[2] = 6;
        campo[3] = 8;
    }
    else{
        campo[2] = 3;
        campo[3] = 5;
    }
}


int  num_possiveis(int mat[MAX][MAX], int m, int n, int ans[])
{
    int i, j, k;
    int  campo[4];
    int flag = 0;
    for (i = 0; i <= MAX;i++)
        ans[i] = 1;
    ans[0] = 0;
    for (i = 0; i < MAX; i++){
        ans[mat[i][n]] = 0; /* Mesma coluna */
        ans[mat[m][i]] = 0; /* Mesma linha */ 
    }
    fcampo(m, n, campo);
    for (j = campo[0]; j <= campo[1]; j++)
        for (k = campo[2]; k <= campo[3]; k++){
            ans[mat[j][k]] = 0; /* Mesmo campo */ 
        }
    for (i = 0; i <= MAX; i++)
        if (ans[i] == 1)
            flag = 1;
    return flag;

}

void copia(int in[][MAX],int m, int n, int out[][MAX])
{
    int i, j;
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            out[i][j] = in[i][j];
}
  

/*
 * Funcao: resolve
 * Resolve o Sudoku da matriz resposta.
 * Retorna 1 se encontrar uma resposta, 0 caso contrario
 */
int resolve(int resposta[MAX][MAX]) {
    int i, j, k;
    int won = 0;
    int possiveis[10];
    int flag;
    int vcopia[MAX][MAX];
    /* int vcopia[MAX][MAX]; */
    /* copia(resposta, MAX, MAX, vcopia); */
    /* print_sudoku(vcopia); */
    for (i = 0; i < MAX && won == 0; i++)
        for (j = 0; j < MAX && won == 0; j++){
            if (resposta[i][j] <= 0){
                flag = num_possiveis(resposta, i, j, possiveis);
                if (flag ){
                    for (k = 1; k <= MAX && !won; k++){
                        if (possiveis[k]){
                            resposta[i][j] = k;
                            won = resolve(resposta);
                            if (!won)
                                resposta[i][j] = 0;
                        }
                    }
                }
            }
        }
    print_sudoku(resposta);
    if (won)
        return 1;
    else
        return 0;
}
