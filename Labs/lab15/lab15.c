/* Laboratorio 15 - Banco de Dados Geografico
 * Nome: Rafael Gonçalves
 * RA: 186062
 * Objetivos: Implementar funções para a consulta de dados sobre um banco
 * de dados de cidades.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    float x;
    float y;
} Ponto;

typedef struct {
    Ponto coordenadas;
    int inicioCEP;
    int fimCEP;
    int numHabitantes;
} Cidade;

/*
 * Funcao: distancia
 *
 * Parametros:
 *   a, b: pontos
 *
 * Retorno:
 *   A distancia euclidiana entre a e b.
 */
float distancia(Ponto a, Ponto b) {
    return floor(100.0 * sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))) / 100.0; 
}

/* Funcao: consulta_cidade_por_cep
 *
 * Parametros:
 *   cidades: vetor de cidades (base de dados) 
 *       cep: CEP desejado
 *         n: tamanho do vetor de cidades 
 * 
 * Retorno:
 *   O indice da cidade que contem o CEP desejado, ou -1 se nao houver. 
 */
int consulta_cidade_por_cep(Cidade* cidades, int cep, int n) {
    int i, ans = -1;
    for (i = 0; i < n; i++)
        if (cidades[i].inicioCEP < cep && cep < cidades[i].fimCEP)
            ans = i;
    return ans;
}

/* Funcao: consulta_cidades_por_raio
 *
 * Parametros:
 *            cidades: vetor de cidades (base de dados) 
 *   cidadeReferencia: indice da cidade referencia (centro da circunferencia)
 *               raio: raio da busca
 *                  n: tamanho do vetor de cidades
 *           nRetorno: deve ser modificado para conter o tamanho do vetor de retorno
 *
 * Retorno:
 *   A sua funcao deve alocar dinamicamente um vetor resposta contendo os indices das cidades
 *   dentro do raio especificado (incluindo a cidade referencia). A função deve devolver 
 *   este vetor *ordenado pelas respectivas distancias da cidade referencia*.
 *   A funcao deve ainda setar o valor nRetorno com o tamanho do vetor.
 */

/* bubble sort comparando distância*/
int *bsort1(Cidade *cidades, int *ls, int len, float* d)
{
    int i;
    int aux, swap = 0;
    do {
        swap = 0;
        for (i = 1; i < len; i++){
            if ( d[ls[i-1]] > d[ls[i]]){
                aux = ls[i];
                ls[i] = ls[i-1];
                ls[i-1] = aux;
                swap = 1;
            }
        }
    }
    while(swap);
    /* Sair do loop quando não houver mais necessidade de trocar nenhum eelemento */

    return ls;
}

int* consulta_cidades_por_raio(Cidade* cidades, int cidadeReferencia, float raio, int n, int* nRetorno) {
    int i, k = 0;
    float* d = malloc(n*sizeof(int));
    int* ans = malloc(n*sizeof(int));
    for (i = 0; i < n; i++)
        if ((d[i] = distancia(cidades[i].coordenadas, cidades[cidadeReferencia].coordenadas)) < raio){
            ans[k++] = i;
        }
    *nRetorno = k;
    ans = bsort1(cidades, ans, *nRetorno, d);
    free(d);
    d = NULL;
	return ans;
}

/* Funcao: populacao_total
 *
 * Parametros:
 *            cidades: vetor de cidades (base de dados) 
 *   cidadeReferencia: indice da cidade referencia (centro da circunferencia)
 *               raio: raio da busca
 *                  n: tamanho do vetor de cidades
 *          
 * Retorno:
 *   O numero de habitantes das cidades que estao contidas no raio de busca
 */
int populacao_total(Cidade* cidades, int cidadeReferencia, float raio, int n) {
    int i, ans = 0;
    int *indices = malloc(n*sizeof(int));
    int *nRet = malloc(sizeof(int));
    indices = consulta_cidades_por_raio(cidades, cidadeReferencia, raio, n, nRet);
    for (i = 0; i < *nRet; i++)
        ans += cidades[indices[i]].numHabitantes;
    free(nRet);
    nRet = NULL;
    free(indices);
    indices = NULL;
	return ans;
}


/* Funcao: mediana_da_populacao
 *
 * Parametros:
 *            cidades: vetor de cidades (base de dados) 
 *   cidadeReferencia: indice da cidade referencia (centro da circunferencia)
 *               raio: raio da busca
 *                  n: tamanho do vetor de cidades *          
 * Retorno:
 *   Mediana do numero de habitantes das cidades que estao contidas no raio de busca
 */

/* bubble sort comparando o numero de habitantes das cidades*/
int *bsort2(Cidade *cidades, int *ls, int len)
{
    int i = 0;
    int aux, swap;
    do {
        swap = 0;
        for (i = 1; i < len; i++){
            if (cidades[ls[i-1]].numHabitantes > cidades[ls[i]].numHabitantes){
                aux = ls[i];
                ls[i] = ls[i-1];
                ls[i-1] = aux;
                swap = 1;
            }
        }
    }
    while (swap);

    return ls;
}

float mediana_da_populacao(Cidade* cidades, int cidadeReferencia, float raio, int n) {
    float  ans = 0;
    int *nRet = malloc(sizeof(int));
    int *indices = malloc(n*sizeof(int));
    indices = consulta_cidades_por_raio(cidades, cidadeReferencia, raio, n, nRet);
    indices = bsort2(cidades, indices, *nRet);
    if (*nRet % 2 == 0)
        /* mediana é a media dos dois termos do meio */
        ans = (cidades[indices[*nRet/2]].numHabitantes + cidades[indices[*nRet/2 - 1]].numHabitantes)/2.0;
    else
        /* mediana é o termo do meio */
        ans = cidades[indices[*nRet/2]].numHabitantes;
    free(nRet);
    free(indices);

	return ans;
}
