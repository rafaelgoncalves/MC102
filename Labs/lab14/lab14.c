/* Nome: Rafael Gonçalves
 * RA: 186062
 * Objetivos: Implementar funções para lidar com conjuntos usando ponteiros e struct */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int *numeros; /* Vetor com elementos, pode ser ordenado ou nao. */
  int elementos; /* Quantidade de elementos distintos */
  int capacidade; /* Tamanho do vetor 'numeros' para gerenciar alocacao de memoria */
} conjunto;

/* Funcao: novo
 *
 * Cria um novo conjunto vazio (sem elementos) com um vetor numeros de determinada capacidade.
 * O conjunto deve ser alocado dinamicamente assim como o vetor elementos do mesmo.
 *
 * Parametros:
 *   capacidade: tamanho do vetor numeros que deve ser alocado
 *
 * Retorno:
 *   O endereço do novo conjunto alocado.
 *   A funcao deve ainda setar os campos elementos e capacidade corretamente.
 */
conjunto *novo(int capacidade) {
    conjunto *novo = malloc(sizeof(conjunto));
    novo->numeros = (int *) malloc(capacidade * sizeof(int));
    novo->elementos = 0;
    novo->capacidade = capacidade;
    
    return novo;
}

/* Funcao: aumenta_capacidade
 *
 * Aumenta a capacidade de um determinado conjunto.
 * Deve-se criar um novo vetor numeros com a capacidade especificada e trocar com o anterior,
 * onde, neste caso, lembre-se de liberar o vetor antigo que nao esta mais em uso.
 * O ponteiro do conjunto nao deve ser modificado, apenas seus dados internos.
 *
 * Parametros:
 *   conj: ponteiro do conjunto que tera a capacidade aumentada
 *   capacidade: nova capacidade
 *   A funcao deve setar os campos elementos e capacidade corretamente.
 */
void aumenta_capacidade(conjunto *conj, int capacidade)
{
    int i, *aux;
    aux = (int *) malloc((conj->elementos) * sizeof(int));
    for (i = 0; i < (conj->elementos); i++)
        aux[i] = (conj->numeros)[i];
    free(conj->numeros);
    conj->numeros = malloc(capacidade * sizeof(int));
    for (i = 0; i < (conj->elementos); i++)
        (conj->numeros)[i] = aux[i];
    free(aux);
    aux = NULL;
    conj->capacidade = capacidade;
    return;
}

/* Funcao: deleta
 *
 * Libera a memoria alocada para o vetor elementos de um determinado conjunto.
 * A função deve também liberar a memória alocada para o conjunto.
 *
 * Parametros:
 *   conj: ponteiro do conjunto a ser deletado
 */
void deleta(conjunto *conj) {
    free(conj->numeros);
    conj->numeros = NULL;
    free(conj);
    conj = NULL;
    return;
}

/* Funcao: pertence
 *
 * Verifica se o conjunto apontado por conj possui o elemento num.
 * O conjunto nao deve ser modificado.
 *
 * Parametros:
 *   conj: ponteiro do conjunto de entrada
 *   num: elemento a ser verificado
 *
 * Retorno:
 *   1 se num pertence a conj, e 0 caso contrario
 */
int pertence(conjunto *conj, int num) {
    int i, ans = 0;
    for(i = 0; i<(conj->elementos); i++)
        if(num == (conj->numeros[i]))
            ans = 1;
    return ans;
}

/* Funcoes: contido
 *
 * Verifica se o conjunto apontado por conj1 esta contido no conjunto apontado por conj2.
 * Os conjuntos nao devem ser modificados.
 *
 * Parametros:
 *   conj1: ponteiro do conjunto de entrada que sera o primeiro operando
 *   conj2: ponteiro do conjunto de entrada que sera o segundo operando
 *
 * Retorno:
 *   0 se o conjunto 1 nao esta contido no conjunto 2,
 *   qualquer valor diferente caso contrario.
 */
int contido(conjunto *conj1, conjunto *conj2) {
    int i, ans = 1;
    for (i = 0; i < (conj1->elementos); i++)
        if (!pertence(conj2,  (conj1->numeros)[i]))
            ans = 0;
    return ans;
}


/* Funcoes: adicao
 *
 * Inclui um elemento no conjunto sem permitir duas ou mais copias do mesmo elemento.
 * A quantidade de elementos no conjunto deve ser atualizada se o elemento for inserido.
 * Pode ser necessario aumentar a capacidade do conjunto.
 *
 * Parametros:
 *   conj: ponteiro do conjunto de entrada
 *   num: elemento a ser incluido
 */
void adicao(conjunto *conj, int num) {
    if (!pertence(conj, num)){
        /* Se não pertence, adiciona */
        if ((conj->elementos) >= (conj->capacidade))
            aumenta_capacidade(conj, (conj->capacidade)+1);
        (conj->numeros)[(conj->elementos)]  = num;
        (conj->elementos)++;
    }
    return;
}

/* Funcoes: subtracao
 *
 * Remove um elemento de um conjunto apontado por conj.
 * A quantidade de elementos no conjunto deve ser atualizada se o elemento for removido.
 *
 * Parametros::
 *   conj: ponteiro do conjunto de entrada
 *   num: elemento a ser removido
 */

void subtracao(conjunto *conj, int num) {
    int i, j = 0;
    if (pertence(conj, num)){
        /* Se pertence subtrai */
        for (i = 0; j < (conj->elementos);i++){
            (conj->numeros)[j++] = (conj->numeros)[i];
            if ((conj->numeros)[i] == num){
                j--;
                (conj->elementos)--;
            }
        }
    }
    return;
}


/* Funcoes: uniao, intersecao, diferenca
 *
 * Realiza a operacao do nome da funcao entre o conjunto conj1 e o conj2, respectivamente.
 * O resultado deve ser armazenado num novo conjunto alocado para esta finalidade.
 * Os conjuntos conj1 e conj2 nao devem ser modificados.
 *
 * Parametros:
 *   conj1: ponteiro do conjunto de entrada que sera o primeiro operando
 *   conj2: ponteiro do conjunto de entrada que sera o segundo operando
 *
 * Retorno:
 *   Ponteiro para um novo conjunto que contem o resultado da operacao
 */
conjunto *uniao(conjunto *conj1, conjunto *conj2) {
    int i, j, k = 0;
    conjunto *conjRes = novo((conj2->capacidade));
    for(j = 0; j<(conj2->elementos); j++)
        (conjRes->numeros)[k++] = (conj2->numeros)[j];
    conjRes-> elementos = k;
    /* conjRes = conj1 + con2 */
    for (i = 0; i<(conj1->elementos); i++)
        adicao(conjRes, conj1->numeros[i]);
    return conjRes;
}

conjunto *intersecao(conjunto *conj1, conjunto *conj2) {
    int i;
    conjunto *conjRes;
    conjRes = uniao(conj1, conj2);
    /* conjRes = conj1 u conj2 - (elementos de conj 1 que nao sao de conj 2 e */
    /*                             elementos de conj 2 que nao sao de conj 1) */
    for(i = 0; i<(conj1->elementos); i++)
        if (!pertence(conj2, (conj1->numeros)[i]))
            subtracao(conjRes, conj1->numeros[i]);
    for(i = 0; i<(conj2->elementos); i++)
        if (!pertence(conj1, (conj2->numeros)[i]))
            subtracao(conjRes, conj2->numeros[i]);
  return conjRes;
}
conjunto *diferenca(conjunto *conj1, conjunto *conj2) {
    int i;
    conjunto *conjRes;
    conjRes = uniao(conj2,conj1);
    /* conjRes = conj1 u conj2 - conj2 */
    if ((conj2->elementos) > 0)
        for (i = 0; i<(conj2->elementos); i++)
            subtracao(conjRes, conj2->numeros[i]);
  return conjRes;
}
