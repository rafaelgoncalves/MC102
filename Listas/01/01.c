#include <stdio.h>
#include <math.h>

int e01(void);
int e02(void);
int e03(void);
int e04(void);
int e05(void);
int e06(void);
int e07(void);
int e08(void);
int e09(void);
int e10(void);
int e11(void);

int main(void) {
    int ans;
    printf("Escolha exercício de 1 a 11\n");
    scanf("%d\n", &ans);
    switch (ans) {
        case 1 :
            e01();
        break;
        
        case 2 :
            e02();
        break;
        
        case 3 :
            e03();
        break;
        
        case 4:
            e04();
        break;
        
        case 5 :
            e05();
        break;
        
        case 6 :
            e06();
        break;
        
        case 7 :
            e07();
        break;
        
        case 8 :
            e08();
        break;
        
        case 9 :
            e09();
        break;
        
        case 10 :
            e10();
        break;
        
        case 11 :
            e11();
        break;
        
        default :
            printf("Valor inválido");
    }
}

int e01(void) {
    printf("\nMC102 - Aprendendo a programar em linguagem C\n");
}

int e02(void) {
    printf("\n                MC102\n");
    printf("Aprendendo a programar em linguagem C\n");
}

int e03(void) {
    printf("\n-------------\n");
    printf("-Linguagem C-\n");
    printf("-------------\n");
}

int e04(void) {
    int i;
    printf("\n");
    for (i=0;i<= 10; ++i)
        printf("2    x    %-2d    =    %-2d\n", i, 2*i);
}

int e05(void) {
    float Vkm, Vms;
    scanf("%f", &Vkm);
    Vms = Vkm/3.6;
    printf("\n%f\n", Vms);
}

int e06(void) {
    float b, h, a;
    scanf("\n%f %f", &b, &h);
    a = (b * h)/2;
    printf("%f\n", a);
}

int e07(void) {
    float r, a;
    scanf("\n%f", &r);
    a = 3.14159265359*r*r;
    printf("%f\n", a);
}

int e08(void) {
    float c1, c2, f1, f2;
    scanf("\n%f %f", &c1, &f1);
    f1 = 9*c1/5 + 32;
    c2 = 5*(f2 - 32)/9;
    printf("\nTemp 1\n%3.2fC %3.2fF\n", c1, f1);
    printf("Temp 2\n%3.2fC %3.2fF\n", c2, f2);
}

int e09(void) {
    int a, b, c, d, e;
    float avg;
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
    avg = (a+b+c+d+e)/5;
    printf("\n%f\n", avg);
}

int e10(void) {
    int i;
    float avg = 0, aux;
    for (i = 0; i<5; i++) {
        scanf("\n%f", &aux);
        avg += aux;
    }
    avg /= 5;
    printf("\n%f\n", avg);
}

int e11(void) {
    int x1, x2, y1, y2;
    double d;
    scanf("%d %d\n%d %d", &x1, &y1, &x2, &y2);
    d = sqrt(pow((x1-x2), 2) + pow((y1-y2), 2));
    printf("\n%f\n", d);
}
